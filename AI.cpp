#include "AI.h"
#include "Card.h"
#include <iostream>

	AI::AI(){
		
	/*
		lowestCardValue = 0;
        Card myHighestCard(1,1,0);
        Card myLowestCard(1,1,0);
        Card cardToPlay(1, 1, 0);
        Card highestCardPlayed(1, 1, 0);
    
        indexOfLowest = 0;
        sameSuitCounter = 0;
      */
        
	}

	AI::~AI(){}
	
	std::vector<Card> AI::selectCardsToSwap(std::vector<Card> hand) {
		std::vector<Card> toSwap;
		for(int i = 0; i<3; i++){
			toSwap.push_back(hand.at(i));
		}
		return toSwap;
	}
	
	int AI::selectCard(std::vector<Card> workingHand, std::vector<Card> tableCards){
		int returnCard;
		if (tableCards.size() == 0 && workingHand.size() == 13) { //must play 2C
			for(int i = 0; i < 13; i++) {
				if (workingHand.at(i).getSuit() == 3 && workingHand.at(i).getRank() == 1) {
					returnCard = i;
				}
			}
		}
		
		else if (tableCards.size() == 0) {							// play any card that isn't a heart
			bool cardSet = false;									// unless only has hearts
			for (int i = 0; i < workingHand.size() - 1; i++) {
				if(workingHand.at(i).getSuit() != 2) {
					returnCard = i;
					cardSet = true;
				}
			}
			if (!cardSet) {
				returnCard = (workingHand.size() - 1);
			}
		}
		else {														// play any card that follows suit
			bool cardSet = false;									// unless don't have suit
			for (int i = 0; i < (workingHand.size() - 1); i++) {
				if(workingHand.at(i).getSuit() == tableCards.at(0).getSuit()) {
					returnCard = i;
					cardSet = true;
				}
			}
			if (!cardSet) {
				returnCard = (workingHand.size() - 1);
			}
		}
		return returnCard;
	}

	
	
	
	
	
/*

	std::vector<Card> AI::selectCardsToSwap(std::vector<Card> workingHand){ 
		std::vector<Card> myHand = workingHand; //computer player's hand
		std::vector<Card> cardsToSwap; //to hold computer player's 3 highest cards
		cardsToSwap[0] = myHand[0];
		cardsToSwap[1] = myHand[1];
		cardsToSwap[2] = myHand[2];
		
	
	for(int i = 0; i <= myHand.size()-1; i++){ //finds computer player's 3 highest-valued cards to swap
		Card lowest = cardsToSwap[0];	//holds computer player's lowest-valued card
		int indexOfLowest = 0;	//holds index of the lowest-valued card
		//finds lowest-valued card in cardsToSwap to exchange with a higher-valued card
		for(int j = 0; j <= 2; j++){
			if(cardsToSwap[j].getRank() < lowest.getRank()){
				lowestCardValue = cardsToSwap[j];
				indexOfLowest = j;
			}
		}

		//if a card is found in the computer player's cards that is higher than the cards already in cardsToSwap,
		//replace the lowest-valued card in cardsToSwap with that card
		if((myHand[i].getRank() > cardsToSwap[0].getRank()) || (myHand[i].getRank() > cardsToSwap[1].getRank()) || (myHand[i].getRank() > cardsToSwap[2].getRank())){
			cardsToSwap[indexOfLowest] = myHand[i];
		}
	}
	return cardsToSwap; //returns vectors of cards to swap to another player
}

/*

Card AI::selectCard(vector<Card> workingHand, vector<Card> tableCards, boolean broken){ //selects the card the computer player will play
	myHand = workingHand; //computer player's hand
	heartsBroken = broken;
	cardsOnTable = tableCards; //cards already played by other players
	firstSuit = cardsOnTable[0].getSuit(); //suit of first card played
	
	if(tableCards.size() == 0){	//if computer has to make the first move in a round
		std::vector<Card> notHearts; //to hold all of the computer player's cards that are not hearts
		Card lowestNonHeart = myHand[0]; //holds the computer player's lowest-valued card that is not hearts
		if(myHand.size() == 0){
			for(int i = 0; i < 13; i++) {
				if (myHand.at(i).getSuit() == 3 && myHand.at(i).getRank() == 1)
					cardToPlay = myHand.at(i);
			}
		}
	
		else if(!heartsBroken){ //if hearts has not yet been broken, play the lowest valued card that isn't hearts
			for(int i = 0; i <= myHand.size()-1; i++){ //creates an array of the player's cards that are not hearts
				if(myHand[i].getSuit() != 2){ //is not hearts
					notHearts[i] = myHand[i];
				}
			}
			for(int i = 0; i <= myHand.size()-1; i++){ //finds the lowest valued card that is not hearts
				if(notHearts[i].getRank() < lowestNonHeart.getRank()){
					lowestNonHeart = notHearts[i];
				}
			}

			cardToPlay = lowestNonHeart;
		}

		else{ //if hearts has been broken, play the lowest valued card available
			for(int i = 0; i <= myHand.size()-1, i++){ //determine lowest valued card in hand regardless of suit
				if(myHand[i].number > myLowestCard.number){
					myLowestCard = myHand[i];
				}
			}
			cardToPlay = myLowestCard;
		}
	}

	else{ //if computer is following someone else's move in a round
		Card highestCardPlayed = cardsOnTable[0];	//highest-valued card played so far that is sameSuit; initialized to first card played
		std::vector<Card> sameSuit;//holds all computer player's cards that are firstSuit
		int sameSuitCounter = 0; //keeps track of how many of the computer player's cards are firstSuit
		Card myHighestCard = myHand[0];	//for highest-valued card; initialized to the first of the computer player's cards
		Card myLowestCard = myHand[0]; //for lowest-valued card; initialized to the first of the computer player's cards
		
		for(int i = 0; i <= 2; i++){ //determine the highest-valued card on the table that is the same suit as the suit as the first card played
			if((cardsOnTable[i].getRank() > highestCardPlayed.getRank()) && (cardsOnTable[i].getSuit == firstSuit)){
				highestCardPlayed = cardsOnTable[i];
			}
		}
		for(int i =0; i <= myHand.size()-1; i++){ //creates an array of the player's cards that match the first card's suit
			if(myhand[i].getSuit() == firstSuit){
				sameSuit[i] = currentHand[i];
				sameSuitCounter++;
			}
		}
		for(int i = 0; i <= myHand.size()-1, i++){ //determine highest valued card in hand regardless of suit
			if(myHand[i].getRank() > myHighestCard.getRank()){
				myHighestCard = myHand[i];
			}
		}
		for(int i = 0; i <= myHand.size()-1, i++){ //determine lowest valued card in hand regardless of suit
			if(myHand[i].getRank() < myLowestCard.getRank()){
				myLowestCard = myHand[i];
			}
		}
		if(sameSuitCounter == 0){ //if there are no cards in hand of that hand's suite
			cardToPlay = myHighestCard; //play the highest-valued card
		}
		else{ //if there are cards in hand of that hand's suite
			for(int j = 0; j <= myHand.size()-1; j++){
				if(sameSuit[i].getRank() < cardToPlay.getRank()){
					cardToPlay = sameSuit[i];
				}
			}
		}
		
	}
	return cardToPlay; //returns card to play

	
}



*/