#ifndef AI_H 
#define AI_H 

#include <vector>
#include "Card.h"

class AI{
    public:
        
        AI();~AI();
        
        std::vector<Card> selectCardsToSwap(std::vector<Card> workingHand);

        int selectCard(std::vector<Card> workingHand, std::vector<Card> tableCards);
    
//      private:
//        std::vector<Card> cardsToSwap;
//        std::vector<Card> sameSuit;
    
/*      
        Are these all private too?
        Card lowestCardValue;
        Card myHighestCard;
        Card myLowestCard;
        Card cardToPlay;
        Card highestCardPlayed;
    
        int indexOfLowest;
        int sameSuitCounter;
*/

};
#endif