/*  Title: Card Class
    Author: Carl
    Description: Suits are represented as ints 1 = spades, 2 = hearts, 3 = clubs, 4 = diamonds
                 1=2,...,9=10,10=J,11=Q,12=K,13=A; This is so that comparing cards = comparing numbers
*/

#include "Card.h"
#include <string>      
#include <iostream>

// Constructor
Card::Card(int newRank, int newSuit, int newValue) {
    if (newRank < 1 || newRank > 13)
        std::cout << "Invalid Rank: " << newRank << std::endl;
    if (newSuit > 4 || newSuit < 0) 
        std::cout << "Invalid Suit :" << newSuit << std::endl;
    if (!(newValue == 0 || newValue == 1 || newValue == 13))
        std::cout << "Invalid Point Value: " << newValue << std::endl;
        
    suit = newSuit;
    value = newValue;
    cardRank = newRank;
}

Card::~Card(){}

// accessor methods
int Card::getRank(){
    return cardRank;
}

// 1 = spades, 2 = hearts, 3 = clubs, 4 = diamonds
int Card::getSuit(){
    return suit;
}

int Card::getValue(){
    return value;
}

void Card::displayCard(){
std::cout << "["<< cardRank << "," << suit << ","<< value <<"]" << std::endl;  // e.g., [2,1,1] we have to user display class
    
}

