/*
    Preconditions: Everything should be inlined.
    edited by Carl 10/20/14
                    10/22/14
*/
#ifndef CARD_H
#define CARD_H
class Card{
    private:
        int cardRank;   // 1=2,...,9=10,10=J,11=Q,12=K,13=A; 
        int suit;       // 1=Spades, 3=Clubs, 4=Diamonds, 2=Hearts
        int value;      // Value of the card: 1 if suit = H, 13 if QS, 0 otherwise

    public:
        Card(int newRank, int newSuit, int newValue);

        ~Card();

        int getRank();

        int getSuit();

        int getValue();

        void displayCard();
 };
#endif
