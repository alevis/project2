/*
    Card Handler class
    Uses the deck class 
    MUST COMPILE WITH -std=c++11 OPTION
*/

#include "CardHandler.h"                        //Contains Deck.h and Player.h
//#include "Deck.h"    
//#include "Card.h"
//#include "Player.h"                           //Player.h includes card and vector
//#include <vector>    
//#include <string>                             //Player.h includes string
#include <random>
#include <iostream>

// CONSTRUCTOR
CardHandler::CardHandler() {
    std::string dealerName = "Jimmy";           // Jimmy the dealer 
    Deck jimmysDeck;                            // Don't mess with his deck
}
// DESTRUCTOR
CardHandler::~CardHandler() {}
    
void CardHandler::dealToPlayers(std::vector<Player> &players){ // Pass out 13 Cards to each player
    for(int i=0;i<52;i++){  // For each card in the deck
        //std::cout<<"Adding card number "<<(i+1)<<" to random player."<< std::endl;
        int playerChosenRandomly = rand()%4;    // Pick a random number between 1 and 4
        //std::cout<<"playerChosen: "<<playerChosenRandomly<<std::endl; // DEBUG
        if(players.at(playerChosenRandomly).getHand().size() != 13){
            players.at(playerChosenRandomly).addCard(jimmysDeck.getDeck().at(i));  // Give it to a random player
        }
        else if(players.at((playerChosenRandomly+1)%4).getHand().size() != 13){
            players.at((playerChosenRandomly+1)%4).addCard(jimmysDeck.getDeck().at(i));  // If random player has 13 card, give it to next guy
        }
        else if(players.at((playerChosenRandomly+2)%4).getHand().size() != 13){
            players.at((playerChosenRandomly+2)%4).addCard(jimmysDeck.getDeck().at(i));
        }
        else(players.at((playerChosenRandomly+3)%4).addCard(jimmysDeck.getDeck().at(i)));
            
    }
    std::cout<<"Done adding cards"<<std::endl;
}   


Deck CardHandler::getJimmysDeck(){
    return jimmysDeck;
}
