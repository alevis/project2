#include <string>
#include <vector>
#include "Player.h"             //Card.h 
#include "Deck.h"               

class CardHandler{
    
    private:
        std::string dealerName;  //private by default
        Deck jimmysDeck;
    
    public:
        CardHandler();
        
       ~CardHandler(); //no need for deconstructor, cpp provides a default deconstructor

        void dealToPlayers(std::vector<Player> &players);   // Takes in address of vector?

        Deck getJimmysDeck();
};
