#include <vector>
#include "Card.h"

#ifndef DECK_H
#define DECK_H

class Deck{

    private:
        std::vector<Card> deck;

    public:
        Deck();   
                  
        ~Deck();

        std::vector<Card> getDeck();
};
#endif
