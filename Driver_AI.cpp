/* 
    Test driver for AI class
*/

#include "AI.h"
#include "Player.h"
#include "Deck.h"
#include <assert.h>
#include <vector>
#include <iostream>

using namespace std;


int main(){
    
    AI testAI;
    
    Deck deck;
    vector<Card> cards = deck.getDeck();
    
    Player player1("Player 1", 'c');
    Player player2("Player 2", 'c');
    Player player3("Player 3", 'c');
    Player player4("Player 4", 'c');
    vector<Player> testPlayers;
    testPlayers.push_back(player1);
    testPlayers.push_back(player2);
    testPlayers.push_back(player3);
    testPlayers.push_back(player4);
    
    for(int i = 0; i < 13; i++) {
        player1.addCard(cards.at(4*i));
        player2.addCard(cards.at(4*i + 1));
        player3.addCard(cards.at(4*i + 2));
        player4.addCard(cards.at(4*i + 3));
    } 
    
    Card testCard(1, 1, 0);
    vector<Card> tableCards;
    testCard = player3.getHand().at(testAI.selectCard(player3.getHand(), tableCards));
    assert (testCard.getSuit() == 3);
    assert (testCard.getRank() == 1);
    
    player3.removeCard(0);
    player3.removeCard(0);
    player3.removeCard(0);
    
    Card card1(10, 1, 0);
    Card card2(9, 2, 1);
    Card card3(11, 4, 0);
    
    player3.addCard(card1);
    player3.addCard(card2);
    player3.addCard(card3);
    
    Card testSpade(11, 1, 0);
    Card testHeart(2, 2, 1);
    Card testDiamond(4, 4, 0);
    
    tableCards.push_back(testSpade);
    testCard = player3.getHand().at(testAI.selectCard(player3.getHand(), tableCards));
    assert (testCard.getSuit() == 1);
    
    vector<Card> tableCards2;
    tableCards2.push_back(testDiamond);
    tableCards2.push_back(testSpade);
    testCard = player3.getHand().at(testAI.selectCard(player3.getHand(), tableCards2));
    assert (testCard.getSuit() == 4);
    
    vector<Card> tableCards3;
    tableCards3.push_back(testHeart);
    tableCards3.push_back(testSpade);
    tableCards3.push_back(testDiamond);
    testCard = player3.getHand().at(testAI.selectCard(player3.getHand(), tableCards3));
    assert (testCard.getSuit() == 2);
    
    vector<Card> test3;
    test3 = testAI.selectCardsToSwap(player3.getHand());
    assert (test3.at(0).getSuit() == 3);
    assert (test3.at(1).getSuit() == 3);
    assert (test3.at(2).getSuit() == 3);
    assert (test3.at(0).getRank() == 4);
    assert (test3.at(1).getRank() == 5);
    assert (test3.at(2).getRank() == 6);
    
    cout << "AI class: All assertions pass" << endl;
}