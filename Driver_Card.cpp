/* 
    Test Driver for Card class
    edited by Carl 10/21/14
*/

#include "Card.h"
#include <assert.h>
#include <iostream>

using namespace std;

int main(void){
    // try to construct a valid card of each suit and value

    Card testCard1(1, 4, 0);
    assert (testCard1.getRank() == 1);
    assert (testCard1.getSuit() == 4);
    assert (testCard1.getValue() == 0);

    Card testCard2(13, 2, 1);
    assert (testCard2.getRank() == 13);
    assert (testCard2.getSuit() == 2);
    assert (testCard2.getValue() == 1);

    Card testCard3(11, 1, 13);
    assert (testCard3.getRank() == 11);
    assert (testCard3.getSuit() == 1);
    assert (testCard3.getValue() == 13);

    Card testCard4(5, 3, 0);
    assert (testCard4.getRank() == 5);
    assert (testCard4.getSuit() == 3);
    assert (testCard4.getValue() == 0);

    cout << "Card class: All asserts successful" << endl;

}
