/*
    Test Driver for CardHandler class
    edited by Carl Senecal 10/23/14
*/

#include "CardHandler.h"
#include <iostream>
#include <vector>

using namespace std;

int main() {
    
    vector<Player> players;
 
    Player player1("Impa",'h');
    Player player2("Zelda",'f');
    Player player3("Ganandorf",'h');
    Player player4("Link",'g');
    
    players.push_back(player1);
    players.push_back(player2);
    players.push_back(player3);
    players.push_back(player4);
    
    CardHandler testHandler;
    testHandler.dealToPlayers(players); // Shuffle & Deal (implemented together)
    
    for(int i = 0; i<4; i++) {    // For each player
        cout << "Player["<<i<<"] has Cards:"<< endl;
        for(int j = 0;j<13;j++){     // Debug print his hand
            cout<<(j+1)<<")";
            players.at(i).getHand().at(j).displayCard();
        }
    }
    
    /* 
        DEBUG PRINT: Debug print jimmy's deck
        for(int i=0;i<52;i++){    
        testHandler.getJimmysDeck().getDeck().at(i).displayCard(); 
    }
    */  
    
    /*  DEBUG PRINT: Print each player's names  
        for(int i = 0; i < 13; i++) {
            std::cout << players.at(0).getName() << std::endl;
            std::cout << players.at(1).getName() << std::endl;
            std::cout << players.at(2).getName() << std::endl;
            std::cout << players.at(3).getName() << std::endl;
        }    
     /*
     testHandler.dealToPlayers(players);
     for(int i = 0; i < 13; i++) {
         player1.getHand().at(i).displayCard();
         player2.getHand().at(i).displayCard();
         player3.getHand().at(i).displayCard();
         player4.getHand().at(i).displayCard();
     }*/

}