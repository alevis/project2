/* 
    Test Driver for Deck class
    edited by Carl Senecal 10/23/14
*/

#include "Deck.h"
#include <assert.h>
#include <iostream>

using namespace std;

int main() {
    Deck testDeck;
    std::vector<Card> testCards = testDeck.getDeck();
    
    assert (testCards[0].getRank() == 1);
    assert (testCards[0].getSuit() == 1);
    assert (testCards[0].getValue() == 0);
    
    assert (testCards[51].getRank() == 13);
    assert (testCards[51].getSuit() == 4);
    assert (testCards[51].getValue() == 0);
    
    cout << "Deck class: All asserts successful" << endl;
}
    