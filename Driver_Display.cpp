/* Title: Project 1 ECE373 Example driver artifact for Display class
 * Author: John Shield
 * Description: Demonstrates Unix Signal capturing and the Display class
 *		functionality.
 *
 * NOTES:
 *      * Requires the terminal (Putty) to be set to UTF-8.
 *      * Does not function when running a screen session.
 */
 
#include "Display.h"
#include "Player.h"
#include "CardHandler.h"
#include "Deck.h"
#include <signal.h>
#include <ncurses.h>
#include <cstdlib>
#include <sstream>
#include <vector>

using namespace std;

//bool validmove(int getX,int getY,){
//
//}

/* No Header file for this example driver artifact
 * function declaration here instead.
 */
// Signal Subroutine for Window Resize
static void detectResize (int sig); 
// stub artifact for what the game does when the screen resizes
void stub_PrintResize(void); 

// The gameDisplay object is global, because the static signal handler object
// needs to access the dynamic object.
display gameDisplay;

/*
 * This is the main function that starts the driver artifact.
 * This function demonstrates some of the abilities of the Display class
 */
int main(int argc, char* argv[])
{
	
	CardHandler testHandler;

	std::vector<Card> testTable2;

	std::vector<Player> testPlayers;
	Player player1("p one", 'h');
	Player player2("p two", 'f');
	Player player3("p three", 'h');
	Player player4("p four", 'f');
	testPlayers.push_back(player1);
	testPlayers.push_back(player2);
	testPlayers.push_back(player3);
	testPlayers.push_back(player4);
	
	testHandler.dealToPlayers(testPlayers);

	vector<cardLoc> locations;

	vector<cardLoc> p1cards;
	for(int i = 0; i < 13; i++) {
		cardLoc cLoc;
		cLoc.Right = 78 - 6*i;
		cLoc.Left = 73 - 6*i;
		cLoc.Top = 18;
		cLoc.Bottom = 22;
		cLoc.cardPosition = i;
		cLoc.cardDeleted = false;
		p1cards.push_back(cLoc);
	}

	cardLoc locations1;
	cardLoc locations2;
	cardLoc locations3;
	cardLoc locations4;

	locations.push_back(locations1);
	locations.push_back(locations2);
	locations.push_back(locations3);
	locations.push_back(locations4);

	locations1.Left = 1;
	locations1.Right = 78;
	locations1.Top = 18;
	locations1.Bottom = 22;

	locations2.Left = 0;
	locations2.Right = 5;
	locations2.Top = 1;
	locations2.Bottom = 17;

	locations3.Left = 24;
	locations3.Right = 53;
	locations3.Top = 1;
	locations3.Bottom = 5;

	locations4.Left = 73;
	locations4.Right = 78;
	locations4.Top = 18;
	locations4.Bottom = 22;

	// using a stringstream rather than a string to make making the banner easier
	stringstream messageString;

	// various variable declarations
	char key;
	int cardX = 0;
	int cardY = 0;
	int suit = 0;
	int number = 0;

	int dragX = 0;
	int dragY = 0;

	// enable a interrupt triggered on a window resize
	//signal(SIGWINCH, detectResize); // enable the window resize signal

	gameDisplay.redrawHands(testPlayers);
	gameDisplay.pickCard(testPlayers);

	vector<Card> testTable;
	Card ntestCard1(1, 1, 0);
	Card ntestCard2(2, 1, 1);
	Card ntestCard3(3, 3, 0);
	Card ntestCard4(4, 4, 0);
	testTable.push_back(ntestCard1);
	testTable.push_back(ntestCard2);
	testTable.push_back(ntestCard3);
	testTable.push_back(ntestCard4);

gameDisplay.pickCard(testPlayers);
gameDisplay.drawTable(testTable, 0);
testPlayers.at(0).playCard(testTable2);
testPlayers.at(0).playCard(testTable2);
testPlayers.at(0).playCard(testTable2);
testPlayers.at(0).playCard(testTable2);
testPlayers.at(0).playCard(testTable2);
testPlayers.at(0).playCard(testTable2);
/*
gameDisplay.pickCard(testPlayers);
gameDisplay.drawTable(testTable, 1);
gameDisplay.pickCard(testPlayers);
gameDisplay.drawTable(testTable, 2);
gameDisplay.pickCard(testPlayers);
gameDisplay.drawTable(testTable, 3);
gameDisplay.pickCard(testPlayers);
gameDisplay.pickCard(testPlayers);
gameDisplay.pickCard(testPlayers);
gameDisplay.pickCard(testPlayers);
gameDisplay.pickCard(testPlayers);
gameDisplay.pickCard(testPlayers);
gameDisplay.pickCard(testPlayers);
gameDisplay.pickCard(testPlayers);
gameDisplay.pickCard(testPlayers);
gameDisplay.pickCard(testPlayers);
*/

gameDisplay.clearScreen();
for(;;) {
}



/* You can uncomment and change the colors for various cards here*/
//    init_pair(1, COLOR_CYAN, COLOR_BLACK); // for card outline
//    init_pair(2, COLOR_BLUE, COLOR_BLACK); // for spades and clubs
//    init_pair(3, COLOR_RED, COLOR_BLACK);  // for hearts and diamonds
//    init_pair(4, COLOR_GREEN, COLOR_BLACK); // for turned over card
//    init_pair(5, COLOR_GREEN, COLOR_BLACK); // for box drawing
//    init_pair(6, COLOR_GREEN, COLOR_BLACK); // for banner display


	// infinite loop for the main program, you can press ctrl-c to quit
	for (;;) {
		// calls the game display to capture some input
    	key = gameDisplay.captureInput();
		// if a mouse event occurred
		if (key == -1) {
			// make a banner message
			messageString.str("");
			messageString << "A mouse event occurred x=" \
				<< gameDisplay.getMouseEventX() << ", y=" \
				<< gameDisplay.getMouseEventY() << ", bstate=" \
				<< gameDisplay.getMouseEventButton();
			// display a banner message
			gameDisplay.bannerTop(messageString.str());
			// record the location of the mouse event
			cardX = gameDisplay.getMouseEventX();
			cardY = gameDisplay.getMouseEventY();

			if ((locations1.Left <= cardX) && (cardX <= locations1.Right) && (locations1.Bottom >= cardY) && (cardY >= locations1.Top))
					gameDisplay.bannerBottom("Valid Player 1 Click");
			else {
				gameDisplay.bannerBottom("Invalid Player 1 Click");
				gameDisplay.clearTable();
			}
			// Some of the mouse click values are defined in display.h
			// check if it was a left click
		/*	if (gameDisplay.getMouseEventButton()&LEFT_CLICK) {
				// draw a random card at the click location
				suit = rand()%5;
				number = rand()%15;
				gameDisplay.displayCard(cardX,cardY,suit,number, A_BOLD);
			// check if it was a right click
			} else if (gameDisplay.getMouseEventButton()&RIGHT_CLICK) {
				// erase a portion of the screen in the shape of a card
				gameDisplay.eraseBox(cardX,cardY,6,5);
			// check for the start of a drag click
			} else if (gameDisplay.getMouseEventButton()&LEFT_DOWN) {
				// record start of the drag
				dragX = cardX;
				dragY = cardY;
			// when the mouse is released
			} else if (gameDisplay.getMouseEventButton()&LEFT_UP) {
				// calculate size of the drag
				int sizeX = abs(dragX-cardX);
				int sizeY = abs(dragY-cardY);
				// get to the top left corner of the drag area
				if (dragX > cardX)
					dragX = cardX;
                if (dragY > cardY)
                    dragY = cardY;
				// draw a box around the drag area
				gameDisplay.drawBox(dragX, dragY, sizeX, sizeY, 0);
			}
		// if a key was pressed
		} else if(key > 0) {
			// make bottom a banner message saying that a key was pressed
			messageString.str("");
			messageString << "Key " << key << " pressed";
			gameDisplay.bannerBottom(messageString.str());
		*/
		}
	}

	return 0;
}

/*
 * This is the interrupt service routine called when the resize screen 
 * signal is captured.
 */
void detectResize(int sig) {
	// update the display class information with the new window size
    gameDisplay.handleResize(sig);
	// re-enable the interrupt for a window resize
    signal(SIGWINCH, detectResize);
	/*INSERT YOUR OWN SCREEN UPDATE CODE instead of stub_PrintResize*/
	stub_PrintResize();
}

/*
 * This is a simple stub that should be replaced with what the game does
 * when the screen resizes. 
 */
void stub_PrintResize(void) {
	// gets the new screen size
	int cols = gameDisplay.getCols();
	int lines = gameDisplay.getLines();
	// setups a message stream
	stringstream messageString;
	messageString << "Terminal is " << cols << "x" << lines;
	// prints out the information of the new screen size in a top banner
	gameDisplay.bannerTop(messageString.str());
}


