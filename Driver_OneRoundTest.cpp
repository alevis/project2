#include <iostream>
#include <vector>
#include <assert.h>
#include "Card.h"
#include "Deck.h"
//#include "CardHandler.h"
#include "Player.h"
#include "AI.h"
#include "Round.h"
#include "Display.h"

int main(){

	vector<Player> testPlayers;	// Initialize 4 players and store in vector
	Player p1("Andrew",'h');
	Player p2("Bob",'b'); 
	Player p3("Cory",'b');
	Player p4("Dave",'b');
	testPlayers.push_back(p1);
	testPlayers.push_back(p2);
	testPlayers.push_back(p3);
	testPlayers.push_back(p4);


	int roundCounter = 1; 				// Start at round 1
   	Round curRound(roundCounter, testPlayers, 1);	// Create a round object
	curRound.jimmyShuffleDeal(testPlayers); 	
	curRound.playRound(roundCounter, testPlayers, 1); // Play through the round
	

	return 0; 
}