/*
    Test driver for Player class
*/

#include "Player.h"
#include "Card.h"
#include <assert.h>
#include <iostream>
#include <string>

using namespace std;

int main(){
	Player testPlayer1("testPlayer1", 'f');       // default constructor: comp player
	Player testPlayer2("testPlayer2", 'f');  // secondary constructor: computer
	Player testPlayer3("testPlayer3", 'h');   // secondary constructor: human

	assert (testPlayer1.getName() == "testPlayer1");
	assert (testPlayer2.getName() == "testPlayer2");
	assert (testPlayer3.getName() == "testPlayer3");

	assert (testPlayer1.isHuman() == false);
	assert (testPlayer2.isHuman() == false);
	assert (testPlayer3.isHuman() == true);


	Card testCard1(1, 1, 0);
	Card testCard2(2, 2, 1);
	Card testCard3(3, 3, 0);
	testPlayer1.addCard(testCard1);
	testPlayer1.addCard(testCard2);
	assert (testPlayer1.getHand().at(0).getRank() == testCard1.getRank());
	assert (testPlayer1.getHand().at(1).getSuit() == testCard2.getSuit());
	assert (testPlayer1.getHand().size() == 2);
	assert (testPlayer1.cardsLeft() == 2);
	testPlayer1.removeCard(0);
	assert (testPlayer1.cardsLeft() == 1);
	assert (testPlayer1.getHand().at(0).getValue() == testCard2.getValue());
	testPlayer1.removeCard(0);
	assert (testPlayer1.cardsLeft() == 0);

	cout << "Player class: All assertions successful" << endl;
	
	return 0;	
}

