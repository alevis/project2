#include <iostream>
#include <vector>
#include "Round.h"

using namespace std;

int main(){
    Player gandalf("Gandalf",'h'); 
    Player jordan("Jordan",'h');
    Player watson("Watson",'f');
    Player hal("Hal",'g');
    
    //////////////////////////////////////////////////////////
    ///         Test case1: isValidPlay()
    /////////////////////////////////////////////////////////
    
    Card hearts(1,1,1);Card spades(2,1,1);
    Card clubs(3,3,0); Card diamonds(4,2,0)
    
    vector<Player> players;
    vector<Card> table;
    
    players.push_back(gandalf);players.push_back(jordan);
    players.push_back(watson);players.push_back(hal);
    
//    table.push_back(hearts); table.push_back(clubs);
//    table.push_back(spades); table.push_back(diamonds);
    
    Card playedCard(1,1,1);
    
    int rotationum = 0, roundcount = 0, rotationstart = 0;
    
    
    Round round1(roundcount, players,rotationstart)<<endl;
    
    //This should return false
    cout<<round1.isValidPlay(playedCard,table,gandalf)<<endl;
    
    Card playedCard = Card newCard(3,2,0);
    
    //This should return true
    cout<<round1.isValidPlay(playedCard,table,gandalf)<<endl;
    
    
    //////////////////////////////////////////////////////////
    ///         Test case2: isValidPlay()
    /////////////////////////////////////////////////////////    
    
    return 0; 
}