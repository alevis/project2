/* Title: HeartsGameEngine Driver
 * Author: Carl, Levis, Minh
 * Description: This class will drive the game. 
 *
 * NOTES: 
 * 1) Code in this file should always compile in VM! (Top-down integrate)
 * 2) #include <random> requires that you compile HeartsGameEngine.cpp using the following statement:
 *          g++ -std=c++11 -c HeartsGameEngine.cpp
 */

// PREPROCESSOR DIRECTIVES
#include <iostream>
#include <string>
#include <random>
#include <vector>

#include <signal.h>
#include <ncurses.h>
#include <cstdlib>
#include <sstream>

#include "Deck.h"                      
#include "Round.h"  
#include "Display.h"            

#define TABLESIZE 4
#define ROTATIONS 13
#define MAXSCORE 100

using namespace std;

// MAIN METHOD DECLARATIONS
//void displayWinner(); 

// GLOBAL VARIABLES
int roundCounter = 1;           // Round counter variable for main
bool gameOver = false;          // This should be set by Round      
vector<Player> gamePlayers;     // To initalize the 4 players 
vector<Round> gameRounds;       // Stores round objects; **gameRound starts at index 1 not at index 0; null is stored at gameRound[0]** 
display gameDisplay;		

void initializePlayers(){     // Create player objects
    string p1Name;
    cout << "Enter player 1 name: " << endl; 
    cin >> p1Name; 
    gamePlayers.push_back(Player(p1Name,'h'));
    gamePlayers.push_back(Player("Computer2", 'b'));
    gamePlayers.push_back(Player("Computer3", 'b'));
    gamePlayers.push_back(Player("Computer4", 'b')); 
}
                                // Creates a vector of 4 players
void addToGameRounds(Round pushMeBabyOneMoreTime, int roundCounter){
    gameRounds[roundCounter] = pushMeBabyOneMoreTime; 
}         // Function that stores round objects into gameRounds


main(){
    /* **NOTE** Before main menu, game engine should know:
       1) (MULIPLAYER) How many computers to poll from (from which we can deduce the number of human players to set up the game)
       2) (STATEFUL) If data stored in persistent memory indicates that there was a previous game; If it indicates a previous
          game then: 
          a. What stage of the game was it in? 
          b. Who's turn is it? 
          c. Has a heart-card been broken?
          d. What did the table look like?
          e. What cards did each player have?
          f. What was each person's roundScore? What was each person's totalScore?
    */

    /* INITIALIZATION
     */
	char mainMenuInput; 
	
    
    /* MAIN MENU    
     */
    cout << "Press N for new game or any other key to exit." << endl;
    cin >> mainMenuInput;

    if(mainMenuInput=='N'||mainMenuInput=='n'){

	initializePlayers();

        Round curRound(roundCounter, gamePlayers, 1);       // Create first round; roundCounter = 1, gamePlayers have empty hands, 1 = rotation starts at 1 and goes to 13       
	gamePlayers = curRound.jimmyShuffleDeal(gamePlayers);
	curRound.playRound(roundCounter, gamePlayers, 1);   // Let the players play out the round; Scores should be updated at this point
		cout << "HERE" << endl;         
	addToGameRounds(curRound, roundCounter);            // Add the round to the data structure that stores all rounds 
        
        while(!gameOver){   // While game isn't over
            roundCounter++;                                                                     // Increment roundCounter 
            Round nextRound(roundCounter, gamePlayers, 1);            // Create new Round object; each player should have his total score field updated; hand should be empty
		curRound = nextRound;           
		curRound.playRound(roundCounter, gamePlayers, 1);  // Play through next round; **gameOver should be set after playRound()**
            addToGameRounds(curRound, roundCounter);                                            // Add the round to the data structure that stores all rounds 
            gameOver = curRound.isGameOver();                                                   // check to see if game is over after round has been played
            
            if(gameOver){   // If after playRound(), game is over
                cout << "Game is over! Someone has score above "<< MAXSCORE << endl; 
                break; // Get out of while loop
            }
        } // Ends while
    }
    
/*    else if(mainMenuInput=='R'||mainMenuInput=='r'){
                    // Reload previous state
    }
*/
    
    else {//if(mainMenuInput=='q'||mainMenuInput=='Q'){
        return 0;   // Exit the program
    }
    
/*    else{
        cout<<"You have entered an invalid character."
    }
*/    
    return 0;
}   // Ends Main


// MAIN METHOD DEFINTIONS



