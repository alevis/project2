/* Title: Round Class 
 * Author: Carl, Levis, Minh
 * Description: 
 *
 */
#include "Round.h"              //Contains Player.h, CardHandler.h 
//using namespace std;            //Can use in .cpp files
#include <vector>
#include <iostream>
#include "Display.h"

extern display gameDisplay;

// Constructor
Round::Round(int roundCount, std::vector<Player> rPlayers, int rotationStart){
	roundPlayers = rPlayers;
    gOver = false;
    isHeartBroken = false; 
    roundNum = roundCount;            // Set round Number for this round object
    rotationStartNum = rotationStart; // Implements statefulness; for now, rotationStart = 1; 
}

Round::~Round(){}

std::vector<Player> Round::jimmyShuffleDeal(std::vector<Player> roundPlayers){
    CardHandler jimmy;  
    jimmy.dealToPlayers(roundPlayers); // Each player in roundPlayers gets 13 cards. The player with 2C should have "leader" variable set true
    return roundPlayers; 
}

void Round::find2clubs(std::vector<Player> &players){
    for (int i = 0; i < 4; i++){
        for (int j = 0; j < 13; j++){
            if (players.at(i).getHand().at(j).getSuit() == 3 && players.at(i).getHand().at(j).getRank() == 1)   // 2C
                players.at(i).leader = true;
            else
                players.at(i).leader = false;
        }
    }
}

int Round::findLeader(std::vector<Player> players){
    for(int i = 0; i < 4; i++){
        if (players.at(i).leader == true)
            return i;
    }
    return -100; // Fails; crash the program
}

bool Round::isValidPlay(Card cardPlayed, std::vector<Card> table, int rotationNum, Player player){
    // first card played must be 2C
    if (rotationNum == 1 && table.size() == 0){ 
            if(cardPlayed.getSuit() == 3 && cardPlayed.getRank() == 2){
                return true;
            }   
            return false;
        }
    // if not the first card to be played:
    else {
        if(cardPlayed.getSuit() == 2){                              // if the player played a heart
            if (!isHeartBroken){                                    // see if hearts is broken
                bool fine = true;                                   // if it isn't, 
                for(int i = 0; i < player.getHand().size(); i++) {  // then see if the player only has hearts
                    if(player.getHand().at(i).getSuit() != 2)           
                        fine = false;                               // if the player has cards other than hearts 
                }                                                   // then this was not a legal move
                if(fine)
                    isHeartBroken = true;
                return fine;                                        // otherwise they only have hearts so it's fine
            }
        }
        else if(table.size() == 0)                      // if no cards have been played and hearts aren't played
            return true;                                // selection is always ok
        else if(table.at(0).getSuit() == cardPlayed.getSuit())    // if the player is not the first to play and follows suit
                return true;                            // then the play is fine
        else {                                          // if the player did not follow suit 
                bool fine = true;                       // check to see if they had the lead suit
                for(int i = 0; i < player.getHand().size(); i++) {          
                    if(player.getHand().at(i).getSuit() == table.at(0).getSuit())
                        fine = false;                   // if they had the lead suit it's a bad play
                }
                if (fine) {
                    if (cardPlayed.getSuit() == 2){
                        isHeartBroken = true;
                    }
                }
                return fine;                            // if they didn't, they can play whatever they want
        }
    }
}

int Round::tableScore(std::vector<Card> table){      
    int tableSubtotal = 0;
    for(int i = 0; i < table.size(); i++){
        tableSubtotal += table.at(i).getValue();
    }
    return tableSubtotal;
}

int Round::tableWinner(std::vector<Card> table){
    int leadSuit = table.at(0).getSuit();
    int leader = 0;
    for(int i = 1; i < 4; i++) {
        if (table.at(i).getSuit() == leadSuit){ // If card played is same suit
            if (table.at(i).getRank() > table.at(leader).getRank()) {
                leader = i;
            }
        }
    }
    return leader;
}

//round does swapping

void Round::swapCards(std::vector<Player> &players,int state){

	std::cout << players.at(0).getHand().size() << std::endl;

	//gameDisplay.redrawHands(players);

	std::cout << players.at(0).getHand().size() << std::endl;
	std::cout << players.size() << std::endl;

     std::vector<Card> temp;
     std::vector<Card> temp1;
     std::vector<Card> temp2;
     if(state == 1){
	for (int i = 0; i < 4; i++){
		players.at(i).choose3();
	}


         //Clockwise
         temp = players.at(0).chosen3;
         players.at(0).chosen3 = players.at(1).chosen3;

         players.at(1).chosen3 = players.at(2).chosen3;    
         players.at(2).chosen3 = players.at(3).chosen3;    
         players.at(0).chosen3 = temp;                     
	players.at(0).updateHand();
	players.at(1).updateHand();
	players.at(2).updateHand();
	players.at(3).updateHand();
     }
     if(state == 2){
         //Counter-clockwise         

	for (int i = 0; i < 4; i++){
		players.at(i).choose3();
	}


         temp1 = players.at(0).chosen3;
         players.at(0).chosen3 = players.at(3).chosen3;    //1 and 4
         players.at(3).chosen3 = players.at(2).chosen3;    //4 and 3
         players.at(2).chosen3 = players.at(1).chosen3;    //3 and 2
         players.at(1).chosen3 = temp;                     //2 and 1

	players.at(0).updateHand();
	players.at(1).updateHand();
	players.at(2).updateHand();
	players.at(3).updateHand();

     }
     if(state == 3){


	for (int i = 0; i < 4; i++){
		players.at(i).choose3();
	}

         //Opposite
         temp2 = players.at(0).chosen3;
         players.at(0).chosen3.swap(players.at(2).chosen3); //1 swaps with 3
         players.at(1).chosen3.swap(players.at(3).chosen3); //2 swaps with 4
	players.at(0).updateHand();
	players.at(1).updateHand();
	players.at(2).updateHand();
	players.at(3).updateHand();
     }
}//end swapCards

void Round::playRound(int roundNumber, std::vector<Player> &players, int rotationNumber) {
    std::cout << "A" << std::endl;
	gameDisplay.redrawHands(players);
    swapCards(players, roundNumber % 4);	
	gameDisplay.redrawHands(players);
    find2clubs(players);
    for(int i = rotationNumber; i <= 13; i++) {
        int leader = findLeader(players); 
        //bool validPlay = false;
        
        for (int j = leader; j < leader + 4; j++) {
	        bool validPlay = false;
            while (!validPlay) {
                Card played = players.at(leader%4).playCard(table);
                validPlay = isValidPlay(played, table, i, players.at(leader%4));
                if (validPlay) {    // Valid card chosen
                    table.push_back(played);
                }
                else {              // Invalid card chosen
                    players.at(leader%4).addCard(played);  // Return card to hand
                }
		gameDisplay.drawTable(table, leader);
            }
        }   // Each player will have put a valid card on table 
        
        int winner = tableWinner(table); 
        int winningPlayer = (winner + leader) % 4;
        // Set leader for next Rotation
        for (int k = 0; k < 4; k++) {
            if (k == winningPlayer)
                players.at(k).leader = true;
            else
                players.at(k).leader = false;
        }
        
        players.at(winningPlayer).roundScore = players.at(winningPlayer).roundScore + tableScore(table);
        
        std::vector<Card> newTable;
        table = newTable;
    }
    
    bool shot = false;
    // Check if someone shot the moon; roundScore == 26
    for (int l = 0; l < 4; l++) {
        if (players.at(l).roundScore == 26)
            shot = true;
    }
    
    if(shot) {
        for (int m = 0; m < 4; m++) {
            if (players.at(m).roundScore == 26)
                players.at(m).roundScore = 0;
            else
                players.at(m).roundScore = 26;
        }
        shot = false;  
    }
    
    // Update each player's total score, reset their roundScore, if a person's total score > MAXSCORE, set gOver = true 
    for(int i = 0; i < 4; i++){
        players.at(i).score += players.at(i).roundScore;
        players.at(i).roundScore = 0;
        if (players.at(i).score > 99)
            gOver = true;
    }
}

bool Round::isGameOver(){
    return gOver; 
}

std::vector<Player> Round::getRoundPlayers(){
    return roundPlayers; 
}
