/* Title: Round Class
 * Author: Minh 
 * Description: The card game of "Hearts" is broken up into rounds. Rounds are numbered (indicated by "roundNum") and this determine swap pattern (see Note 3)
 *              Each round is broken up into 13 rotations (Players play cards until their hands are empty.) In each rotation, 
 *              each player is prompted to place a valid card on the table.
 *              The table is then evaluated for the "winning" player who takes all the card and gets his roundScore updated. This repeats
 *              until 13 rotations are completed. Then we check each player's "moonShot" member to see if he has shot the moon
 *              (i.e., he has all 13 heart-cards and Q,S). We then update each player's "totalScore". The round then ends. 
 *
 *      
 * Note: 
 * 1) In the future, we will use the "roundState" variable to initialize the start of the for-loop that indicates
 *    the particular rotation within the round that the player is in. This implements "Statefulness".
 * 2) roundState will be used to implement statefulness; it should be an int that indicates the rotatation the player is currently in.  ** NOT IMPLEMENTED IN PROJECT2
 * 3) There are 4 swap patterns(SP). SP1: Each player gives 3 of his chosen cards to the player to his left
 *                                   SP2: Each player gives 3 of his chosen cards to the player to his right
 *                                   SP3: Each player gives 3 of his chosen cards to the player on the opposite side of the table
 *                                   SP4: No swapping occurs
 *    Implementation: SP(roundNumber%4)
 */

#include "AI.h"
#include "Player.h"             // This class will manipulate player objects
#include "CardHandler.h"        // CardHandler objected used to shuffle and distribute cards at the start of each round
#include <iostream>             // For debug
#include <vector>

class Round{
    
    public:
    
        bool isHeartBroken; 
        bool gOver;             // Should be set true by playRound at the end of a round if one (or more) player scores exceed 100
        int roundNum;           // Indicates the current round number from which we deduce which swap 
        int rotationStartNum;   // See note 2 
        std::vector<Player> roundPlayers; 
        
        //CONSTRUCTOR/DECONSTRUCTOR
        Round(int roundCount, std::vector<Player> rPlayers, int rotationNumber);   
        ~Round();      
        
        //METHOD DECLARATIONS
        std::vector<Player> jimmyShuffleDeal(std::vector<Player> roundPlayers);        // 1. Create CardHandler Jimmy. 2. Jimmy shuffles and deals hand to each player
	bool isGameOver();        
	
        void playRound(int roundNum, std::vector<Player> &players, int rotationStartNum);      // (IMPL, SPOTCHECKED, UNTESTED)

        
        std::vector<Player> getRoundPlayers();                         // (TRIVIAL) Used after playRound() call to pass player data of this round to the next round

        std::vector<Card> table;

    private: 
        void find2clubs(std::vector<Player> &players);                           // (IMPL, SPOTCHECKED, UNTESTED)
        
        int findLeader(std::vector<Player> players);                               // (IMPL, SPOTCHECKED, UNTESTED)
        
        bool isValidPlay(Card cardPlayed, std::vector<Card> table, int rotationNum, Player player);
        
        int tableScore(std::vector<Card> table);                                   // (IMPL, SPOTCHECKED, UNTESTED)
        
        int tableWinner(std::vector<Card> table);                                  // (IMPL, SPOTCHECKED, UNTESTED)
        
        void swapCards(std::vector<Player> &players, int roundNum);                  // Players exchange cards and hands are updated

        
                                                     // (TRIVIAL) Used after playRound() call; If a player's score is > MAXSCORE. This signal the game's end for while 


};
