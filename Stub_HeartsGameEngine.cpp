/*
    This is HeartsGameEngine.cpp's program stubs. It will include only stubs (no implementation) for all class functions. 
    The format is that of Java's code since most of us can read Java
    We will translate it to CPP after the stub is complete -- implementation discouraged until stub icomplete!
*/

#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
#include <random>

/*======================================================================
GLOBAL VARIABLES & DEFINITIONS
======================================================================*/
#define TABLESIZE 4
#define ROTATIONS 13

using namespace std;

/*===============================================================================================================================
    MAIN METHOD
===============================================================================================================================*/
int main(){

    /* INITIALIZATION
    ======================================================================*/
    Player[4] players = new Player[];                  // Player array to store all players 
    Round gameRounds [1000] = initalizeGameRounds();   /* Data Structure for round objects (primitive "game save" system); one element = one game round (indexed by round #); We need to switch over to a dynamic DS later on; Can't have empty rounds else we have to create methods to traverse the array */
    int rotationStart = 1;                             // Where should rotation start?
    int roundCount = 1;                                // Initialize round counter; This variable should not start from 1 if player chooses to resume game
    gameOver = false;                                  // Lets the while-loop in (n)-mode know when to break and end the game
    
    char mainMenuInput;                                // Main Menu: Get from user, determine new game, resume game, or quit game
    
    initalizePlayerArray();                       // Initalize all player objects
    /* MAIN MENU    
    ======================================================================*/
    cout<<"New game? (n),  Resume game? (r) or (q) to quit."<<endl;
    cin>>mainMenuInput;
    
    if(mainMenuInput=='N'|| mainMenuInput=='n'){   // New game sequence

        players[0].isHuman=true; 
        /* Assume all other players are computer players */
        
        /* GAMEPLAY
        ======================================*/
        Round curRound = new Round(roundCount, players, rotationStartNum);              // FIRST ROUND: Start 1st round; subsequently known as "currentRound"
        curRound.playRound(roundCount, players)                                         // FIRST ROUND: Let players play through the round
        addGameRound(curRound);                                                         // FIRST ROUND: Save 1st round to roundArray (index = roundCount so roundArray[0] = null)
                                                                                        // FIRST ROUND: We dont' need to check for winners
        while(!gameOver){                   // Until the game is over -- create more rounds and play the game
            roundCount++;                   // Increase the roundCounter
            Round curRound = new Round(roundCount, players, rotationStart)
            curRound.playRound(roundCount, players[], rotationStart);                       // Play through the round; This method should ensure that gameOver variable has been properly set
                                                                                            // curRound contains each player's most up-to-date score
            addGameRound(curRound);                                                         // Now that the game has finished, each player's stats contains new info; update this round object in the gameRounds array to update Player objects (and their associated scores)
        
            gameOver=curRound.isGameOver();                                                 // Check current round object's gameOver field to see if game is over
            if(gameOver == true){                                                           // If round object says game over
                break;                                                                      // Break from while loop
            }
        }   // Ends while
        void curRound.displayWiner(gameOver);    // Show everyone's final score and indicate the winner

    }
    else if(mainMenuInput=='R'|| mainMenuInput=='r'){   // Not implemented in Project 2
                    
    }
    else if(mainMenuInput=='q'|| mainMenuInput=='Q'){   // Ezpz
        return 0;   // Exit the program
    }
    else{
        cout<<"You have entered an invalid character."
        main();  // Let's put mainMenu in a while loop rather than calling main .. recursion is unnecessary. 
    }

    
}   // Ends Main

initalizePlayerArray(){
        cout<<"Enter the name of Player 1: "<<endl;   // Get player 1 info
        cin>>players[0].pName;                        // Set player 1 info
}   // Create 4 Player objects, For each: give them default names (Computer1, etc.), set isHuman=false, set cardsLeft=0

Round[] initalizeGameRounds(){}         // Create 1000 "empty" round Objects (a around is empty if roundNum=0), return them as an array
void addGameRound(Round roundToAdd){}   // Store a round into the gameRounds array;
void updateGameRound(Round updatedRound){} // Save a newer version of the same round into the same element in the gameRounds array



/*=====================================================================================================================================================================
MAIN ENDS 
======================================================================================================================================================================*/








//==============================================================================================================================
// ROUND CLASS  
// AUTHOR:
/* DESCRIPTION: 
   A round object will be created for each round of the game. main() will store each round object in an array which is indexed by 
   each round object's round number. An explanantion for why we choose an array will be provided later. Each round in the game of
   Hearts begin with the shuffling and distribution of 52 cards among 4 player. A cardHandler object (named "Jimmy") will accomplish this task. Jimmy
   shuffles the deck (randomizes the order of the cards in the deck) and distributes 13 cards to each player */
//==============================================================================================================================
class Round(){
    
    public int roundNum = 0;        // Indexes round; Round is empty if roundNum = 0
    public bool gameOver = false;   // Should be set true by a round method when game is over
    private int roundState;         // When game is in session, roundState reflects the stage of the game (within the round) that the program is handling
    
    private Player players[];   
    CardHandler Jimmy = new CardHandler();   // CardHandler has the deck, which contains Card objects
    
    /*Round Constructor 
    ======================================================================*/
    Round(int whatRound, Player [] lastRoundsPlayers, bool didGameEnd){
        roundNum = whatRound;        // Initalize this round's roundNumber
        gameOver = didGameEnd;       // Set true if after the completion of last round, the game ended
        players = lastRoundsPlayers; // Set this round's players equal to last round's players (and include last round players' scores, implicitly)
        
        if(!gameOver){
            Jimmy.shuffleDeck();     // Shuffle deck
            Jimmy.dealToPlayers();   // At this point in the beginning of each round, all players should have been dealt 13 card
        }
    }
    
    /*Round Methods
    ======================================================================*/
    **void startRound(int roundState){}   // **Implement as a group ** The game sequence; you start at the stage designated by roundState
    
    void setPlayers(Player p1, Player p2, Player p3, Player p4){}   // Main calls this method to initialize array of players
    int swapType(int roundNumber){}   // Returns the direction to swap: I don't think this is necessary
    void swap(Player players[],int roundState){} // If first round  swap could take in the roundNumber since roundState = swapType(roundNumber) 
    void setRoundState(int newRoundState){}   /* Updates the stage of the round; Rounds are broken into stages of gameplay; We designate a number to each stage so that we can start at that stage in the round if the game should crash*/ 
    void setGameOver(bool tF){}   // Set true when game is over; Used as feed back to the upper while loop
    
    /* ROUND-MAIN COMMUNICATION: Methods that provides data about the round to main() belongs here*/
    bool isGameOver(){}  // Return true if game is over; *Important* also used to determine if game over for next round
    void displayWinner(bool gameOver){} // If this round's gameOver is field is true then print out all player scores, and declare winner

    /* ROUND-ROUND COMMUNICATION: Methods that transfer data between round objects belong here */
    Player[] getPlayers(){}   // Pass  an array of player objects; Used to tranfer data from one round to another (data = player scores)
    void dumpHands(Player []){}   /* Set each player's hand array to _null_ i.e., all elements = null. We don't want players to hold onto cards. 
                                   Sloppy way to do this is to do nothing and to let Jimmy overwrite the elements. */
    
} // Ends round class






//==============================================================================================================================
// PLAYER CLASS
// AUTHOR: 
/* DESCRIPTION: 
*/
//==============================================================================================================================
class Player(){

    private String pName;  
    public bool isHuman;      
    
    public bool isLeader;      // Will this player be the first to play a card in the round
    public int cardsLeft;      // # of cards left in hand
    private Card hand [13];    // This should be ordered by suit, and then by card number; For now just leave them unordered

    /*Player Constructor
    ======================================================================*/
    Player(string name)
    /*Player Methods
    ======================================================================*/

    int cardsLeft(){}       // Returns the number of cards left in player's hands
    void setCardsLeft(int numLeft){}    // Set the number of cards in his hand
    void addToHand(Card newCard){}        // Add card to player's hand[] 
    void removeFromHand(Card targetCard){}   //
}  


//==============================================================================================================================
// COMPUTER CLASS
// AUTHOR: 
/* DESCRIPTION: 
*/
//==============================================================================================================================
class Computer(){
    
    
    
}



//==============================================================================================================================
// CARD HANDLE/"Dealer" CLASS
// AUTHOR: 
/* DESCRIPTION: 
   A cardHandler object will be declared and initialized in the round class. Call him Jimmy. Jimmy
   shuffles the deck (randomizes the order of the cards in the deck) and distributes 13 cards to each player. A cardHandler object
   should, therefore, include a method to pass an array of 13 Card objects given an _ordered_ deck of cards (implemented as an array, see 
   Deck class for convention used). An example of what we want this object to do: 
   We call Jimmy.shuffleDeck() then call Jimmy.dealToPlayers() which does: Jimmy.pass13Cards(Player p1), Jimmy.pass13Cards(Player p2), etc.*/
//==============================================================================================================================
class CardHandler{
    
    private String name = "Jimmy";          // Jimmy the dealer
    private Deck jimmysDeck = new Deck();   // Implemented as an array Deck should be initalized(cards have defined parameters and are ordered) with accessor methods
    
    /*CardHandler Constructor 
    ======================================================================*/
    CardHandler(){
        // Default Constructor: No need to run anything, I think. 
    }
    
    /*CardHandler Methods
    ======================================================================*/
    void shuffleDeck(){}     // Shuffle the card order of the deck 
    void dealTo(Player receiver){}   // Used with dealToPlayers(); Give a player a card from the deck (if deck not empty); *Important*: Update each player's cardLeft field with each pass
    void dealToPlayers(Player p1, Player p2, Player p3, Player p4){}   // Pass out 13 Cards to each player

}






//==============================================================================================================================
// DECK CLASS
// AUTHOR: Minh Tran
/* DESCRIPTION: 
   Deck is an array of Card objects. It's the deck class's job to:
        1) Initalize each card with the appropriate parameters 
        2) Order the cards by value according to suit. Be sure to document the convention you used.  
        3) Provide deck-access methods to CardHandler class
   Note: Use the numbering convention for card numbers defined in Card class; We are not using strings!*/
//==============================================================================================================================
class Deck(){
    
    private Card deck [52];     // Data structure for deck
    private boolean isEmpty;    // true if deck is empty
    
    /*Deck Constructor 
    ======================================================================*/
    Deck(){
        initalizeDeck();   // Define the parameters of each card, for all cards, then order the cards
    }
    
    /*Deck Methods
    ======================================================================*/
    void initalizeDeck(){}  // Initialize each field of each Card object of the deck; Document convention used
    void debugPrintDeck(){} // For testing ; Prints all card (there is a printCard function in Card object)
}    
    
//==============================================================================================================================
// CARD CLASS
// AUTHOR: 
/* DESCRIPTION: 
   We model a card object using parameters that characterizes the card; The 'color' parameter may be used in the display class. 
   Note: Color of a card is implied by the suit and we assume the user knows the card values.*/
//==============================================================================================================================
class Card(){
    
    public int number;     // 1=2,...,9=10,10=J,11=Q,12=K,13=A; This is so that comparing cards = comparing numbers 
    public string suit;    // S=Spades, C=Clubs, D=Diamonds, H=Hearts
    public string color;   // R=Red, B=Black
    public int value;      // Value of the card: 1 if suit = H, 13 if QS, 0 otherwise
    
    public boolean isPlayed; // True if placed on table, False if in player's hand (should not be in deck)
    public boolean isDealt;  // True if passed to a player, False if still in deck
    
    String displayCard(){}   // This method prints to stdout the card in the following format:"[number:suit]" e.g., [3:H], [8:S], [11:D], [13:C]
    
}