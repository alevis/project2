/* Title: CardTester
 * Author: Carl
 * Description: 
 *  - Create a couple of card objects, initialize them, and display them in terminal
 */

#include <iostream>
#include "Card.h"

using namespace std;

int main(void){
    
    // Construct a valid card of each suit and value
    Card testCard1(1, 4, 0);
    Card testCard2(13, 2, 1);
    Card testCard3(11, 1, 13);
    Card testCard4(5, 3, 0);
    // Display the cards
    testCard1.displayCard(); 
    testCard2.displayCard(); 
    testCard3.displayCard(); 
    testCard4.displayCard(); 
    
    return 0; 
}
