/*   Title: Card Class
 *   Author: Carl 
 *   Description: 
 *   - Suit Convention: 1 = spades, 2 = hearts, 3 = clubs, 4 = diamonds
 *   - Rank Convention: 1=2,...,9=10,10=J,11=Q,12=K,13=A
 *   - Value Convention: Queen Spades = 13 pts, Heart Cards = 1 pt. 
 */
#ifndef CARD_H
#define CARD_H

class Card{
    public:
        // Constructor/Destructor
        Card(int newRank, int newSuit, int newValue);   // Rank/Suit/Value
        ~Card();
        
        // Public Methods
        int getRank();      // Returns rank of the card
        int getSuit();      // Returns suit of the card
        int getValue();     // Returns Value of the card
        void displayCard(); // Display the card in the format: [rank,suit,value]
        
    private:
        int cardRank;   // 1=2,...,9=10,10=J,11=Q,12=K,13=A; 
        int suit;       // 1=Spades, 3=Clubs, 4=Diamonds, 2=Hearts
        int value;      // Value of the card: 1 if suit = H, 13 if QS, 0 otherwise
 };

#endif