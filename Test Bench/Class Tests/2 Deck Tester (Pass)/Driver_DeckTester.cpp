/* Title: DeckTester
 * Author: Carl
 * Description: 
 *  - Create a vector of 52 Cards that are initiated properlly, like a regular deck. 
 *  - Creating a deck object automatically creates a vector<Card> of size 52. We test by displaying 52 cards, and then 
 *    by trying to print 53 cards
 *
 *  Compile with: g++ Driver_DeckTester.cpp Deck.cpp Card.cpp -o DeckTester
 */
 
#include <iostream>
#include "Deck.h"

using namespace std;

int main() {
    
    Deck myDeck;                                  // Declare a deck object; A vector<Card> should be initialized
    vector<Card> deck1sDeck = myDeck.getDeck();   // Retrieve the vector<Card> and store it in deck1sDeck variable
    // Print out contents of deck1sDeck
    for(int i=0;i<52;i++){
        cout << i+1 << ") ";
        deck1sDeck[i].displayCard();    // Display card
    }
    
    
    // Print out up to 53 cards (should crash if un-commented) -- NOPE IT PRINTS OUT GARBAGE !!!
    for(int i=0;i<53;i++){
        deck1sDeck[i].displayCard();    // Display card
    }
    
}
    