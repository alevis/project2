/* 
    Player Class
    edited by Carl Senecal 10/23/14
*/
#include <iostream> 
#include "Player.h"						
#include "Display.h"

extern display gameDisplay;

Player::Player(std::string newName, char h){
    name = newName;
    score = 0;
    roundScore = 0;
    leader = false;
    hasPlayed = false;
    trickWinner = false; 
    
	if(h == 'h' ||h == 'H') 
		human = true;
	else
		human = false;
}

Player::~Player(){}

std::string Player::getName(){
    return name;
}
    
std::vector<Card> Player::getHand() {
    return Hand;
}

int Player::cardsLeft(){
    return Hand.size();
}

void Player::addCard(Card newCard){
    Hand.push_back(newCard);
}

Card Player::removeCard(int picked){
	
	Card returnCard(1, 1, 0);
	
	std::vector<Card> newHand;
	
	bool met = false;
	
	for(int j = 0; j < Hand.size()-1; j++) {
		if (picked == j){
			met = true;
			returnCard = Hand.at(j);
		}
		if(met) {
			if (Hand.size() > 1)			
				newHand.push_back(Hand.at(j+1));
		}
		else {
			newHand.push_back(Hand.at(j));
		}
	}

	Hand = newHand;

	return returnCard;
}

bool Player::isHuman(){
    return human;
}

Card Player::playCard(std::vector<Card> tableCards){
	Card returnCard(1, 1, 0);
	if (human == true){
		returnCard = gameDisplay.soloPick(this);
	}
	else{
		int returnIndex = playerAI.selectCard(Hand, tableCards);
		returnCard = Hand.at(returnIndex);
		removeCard(returnIndex);
	}
	return returnCard;
}

void Player::updateHand(){
	for(int i = 0; i < 3; i++)
		Hand.push_back(chosen3.at(i));
}

void Player::choose3(){
	if(human){
		gameDisplay.select3(this);		
	}
	else{
		chosen3 = playerAI.selectCardsToSwap(Hand);
		removeCard(0);
		removeCard(0);
		removeCard(0);
	}
	
}
