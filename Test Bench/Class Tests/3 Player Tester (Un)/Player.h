
#include <vector>
#include <string>
#include "Card.h"

using namespace std;

class Player{
    public:
        //Attribute  
        string name;
        bool isHuman;
        //Gameplay
        bool leader;
        bool has2Clubs;
        bool hasPlayed; 
        bool trickWinner;
        //Scoring
        int rScore;             // A player's round score
        int tScore;             // A player's total score
        
        //Constructor/Destructor
        Player(string newName, char h);
        ~Player();
        
        //Method Declarations 
        vector<Card> getHand();	    // Returns player hand
        
        void choToHand();           // During swap, puts 3 cards from chosen3 back into 
            
        void addCard(Card newCard);
            
    	Card removeCard(int picked);
    	    
    	Card playCard(vector<Card> tableCards);
    	    
    	void choose3();             // Player selects 3 cards from display

    private:
	    vector<Card> hand;	
	    vector<Card> chosen3;       // During swap, 3 cards go here
};
