/* Title:
 * Author: Carl
 * Description: 
 */
 
#include "Deck.h"

Deck::Deck(){
    for(int i = 0; i < 13; i++) {       // for each card rank, make one card

        if (i != 10){                    // S have no point value unless Queen
            Card newCard2(i+1, 1, 0);
            deck.push_back(newCard2);
        } else{
            Card newCard2(i+1, 1, 13);   // Queen of Spades gets 13 points
            deck.push_back(newCard2);
        }

        Card newCard(i+1, 2, 1);       // Hearts all have value 1
        deck.push_back(newCard);

        Card newCard3(i+1, 3, 0);      // Clubs have no point values
        deck.push_back(newCard3);

        Card newCard4(i+1, 4, 0);       // Diamonds have no point values
        deck.push_back(newCard4);
    }
}
Deck::~Deck(){}

vector<Card> Deck::getDeck(){
    return deck;
}
