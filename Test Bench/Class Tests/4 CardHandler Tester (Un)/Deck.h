/* Title:
 * Author: Carl
 * Description: 
 */
 
#include <vector>
#include "Card.h"

using namespace std; 

class Deck{
    public:
        //Construtor/Destructor
        Deck();                     // Creating a deck object initalizes a deck of ordered cards
        ~Deck();

        //Public Methods
        vector<Card> getDeck();     // Returns the deck of ordered cards
        
    private:
        vector<Card> deck;

};

