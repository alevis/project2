#ifndef PLAYER_H
#define PLAYER_H
#include <vector>
#include <string>
#include "Card.h"
#include "AI.h"
//#include "Display.h"


class Player{

    private:
        AI playerAI;
    
        bool human, trickWinner, hasPlayed;
        
        std::string name;
	    std::vector<Card> Hand;		


    public:

        std::vector<Card> chosen3;
        
        Player(std::string newName, char h);
        
        ~Player();

        std::string getName();       
        
        std::vector<Card> getHand();	//returns player hand
        
        bool leader;
        
        bool has2Clubs;
        
        int score; 
        
        int roundScore;  
        
        int cardsLeft();
        
        int getScore();
        
	int getRoundScore();
        
        bool isHuman();
        
        void updateHand();
        
        void addCard(Card newCard);
        
	Card removeCard(int picked);
	    
	Card playCard(std::vector<Card> tableCards);
	    
	void choose3();
};
#endif
