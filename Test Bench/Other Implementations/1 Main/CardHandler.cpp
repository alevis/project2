/*   Title: Card Handler class
 *   Author: Levis, Minh 
 *   Description: 
 *   Notes: Must compile with -std=c++11 
 *   Compile using: g++ CardHandler.cpp Player.cpp Deck.cpp Card.cpp -std=c++11
 */
#include <random>
#include <iostream>
#include "CardHandler.h"                   //Contains Deck.h and Player.h

using namespace std; 

// CONSTRUCTOR/DESRUCTOR DEFINITIONS
CardHandler::CardHandler() {
    string dealerName = "Jimmy";           // Jimmy the dealer 
    
}
    
// METHOD DEFINITIONS
void CardHandler::dealToPlayers(vector<Player> players){ 
    for(int i=0;i<52;i++){                                                                 // For each card in the deck
        //cout << "Adding card number " << (i+1) << " to random player." << std::endl;
        int playerChosenRandomly = rand()%4;                                               // Pick player 1 - 4 randomly. Call him A. A sits next to B,C,D.
        //cout << "playerChosen: " << playerChosenRandomly << endl;                         
        if(players.at(playerChosenRandomly).getHand().size() < 13){                        // If A's hand contains less than 13 cards
            players.at(playerChosenRandomly).toHand(jimmysDeck.getDeck().at(i));           // place the i'th card into his hand
        }                                                                                  // If A's hand contains 13 or more cards
        else if(players.at((playerChosenRandomly+1)%4).getHand().size() < 13){             // Check if B's hand contains less than 13 cards
            players.at((playerChosenRandomly+1)%4).toHand(jimmysDeck.getDeck().at(i));     // place the i'th card into his hand 
        }                                                                                  // If B's hand contains 13 or more cards
        else if(players.at((playerChosenRandomly+2)%4).getHand().size() != 13){            // Check if C's hand contains less than 13 cards
            players.at((playerChosenRandomly+2)%4).toHand(jimmysDeck.getDeck().at(i));     // place the i'th card into his hand 
        }                                                                                  // If C's hand contains 13 or more cards
        else(players.at((playerChosenRandomly+3)%4).toHand(jimmysDeck.getDeck().at(i)));   // placec the i'th card into D's hands
    }
    cout<<"Done adding cards"<<endl;
}   

Deck CardHandler::getJimmysDeck(){
    return jimmysDeck;
}
