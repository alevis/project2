#include <string>
#include <vector>
#include "Player.h"
#include "Deck.h"

using namespace std; 

#ifndef CARDHANDLER_H
#define CARDHANDLER_H

class CardHandler{
    
    private:

    
    public:
        //Attributes
        string dealerName;  
        Deck jimmysDeck;
        //Constructor
        CardHandler();
       ~CardHandler(); 

        void dealToPlayers(vector<Player> players);        // Q. Takes in address of vector?
        Deck getJimmysDeck();                               // Returns intialized deck of cards
};

#endif