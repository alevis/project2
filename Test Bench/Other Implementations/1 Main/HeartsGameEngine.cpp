/* Title: HeartsGameEngine Driver
 * Author: Minh
 * Description: 
 *      This class is another implementation of the game. With this version, I'm taking into consideration
 * all the necessary information that needs to be transferred from one round to another round. That is: 
 *        1) Each player's cumulative score. 
 *        2) Gameplay wise: Round number, Who is the leader, whether hearts has been broken, whether the game has ended. 
 *      For (1) and (2) we can include global variables in main that serves as a place to store that information 
 * at the end of each round. We use those variables to control a while loop that creates a Round object that 
 * takes "who is leader, whether hearts has been broken" booleans into its constructor to facilitate gameplay. We'll use the fact
 * that the end of the while loop, the round object we're working with gets destroyed to reset data and structures that 
 * do not need to persist onto the next round
 *      This all means that we need to create public methods in round that we need to call in the while loop to update 
 * those global variables as they change. Note that hearts broken and who is leader bool should be updated as the game is played 
 * and should be false when a round ends. 
 *
 * NOTES: 
 * 1) Code in this file should always compile in VM! (Top-down integrate)
 * 2) Compile using:
 *          g++ -std=c++11 -o HeartsGame HeartsGameEngine.cpp Card.cpp Deck.cpp CardHandler.cpp Player.cpp Round.cpp
 */

// PREPROCESSOR DIRECTIVES
#include <iostream>
#include <vector>

#include "Deck.h"        // includes Card.h            
#include "CardHandler.h" // includes Player.h
#include "Player.h"
#include "Round.h"

#define TABLESIZE 4
#define ROTATIONS 13
#define MAXSCORE 100

using namespace std;

// GLOBAL VARIABLES
int roundCounter = 1;           // Nonpersistent 
bool heartsBroken = false;      // Nonpersistent 
bool gameOver = false;          // Persistent 
vector<Player> gamePlayers;     // total Score is persistent; Nonpersistent data managed manually
	
main(){

	// Make players
    cout << "Enter player 1 name: " << endl; 
    string p1Name; 
    cin >> p1Name; 
    gamePlayers.push_back(Player(p1Name,'h'));          // 'h'= human  'b' = bot
    gamePlayers.push_back(Player("Computer2", 'b'));
    gamePlayers.push_back(Player("Computer3", 'b'));
    gamePlayers.push_back(Player("Computer4", 'b')); 
    
    // Main menu
    cout << "Press 'n' for new game, 'r' to resume game, 'q' to quit." << endl;
    char mainMenuInput = 'I';
    while(mainMenuInput != 'n' || mainMenuInput != 'r' || mainMenuInput != 'q'){
        cin >> mainMenuInput;
        if(mainMenuInput != 'n' || mainMenuInput != 'r' || mainMenuInput != 'q'){
            cout << "Invalid input. Try again.";
        }
    }
    
    switch(mainMenuInput)
    {
        case 'n':
            /*
            Round curRound(roundCounter, gamePlayers, 1);       // Create first round; roundCounter = 1, gamePlayers have empty hands, 1 = rotation starts at 1 and goes to 13       
    	    gamePlayers = curRound.jimmyShuffleDeal(gamePlayers);
    	    curRound.playRound(roundCounter, gamePlayers, 1);   // Let the players play out the round; Scores should be updated at this point
    	    addToGameRounds(curRound, roundCounter);            // Add the round to the data structure that stores all rounds 
            
            while(!gameOver){   // While game isn't over
                roundCounter++;                                                                     // Increment roundCounter 
                Round nextRound(roundCounter, gamePlayers, 1);            // Create new Round object; each player should have his total score field updated; hand should be empty
    		    curRound = nextRound;           
    		    curRound.playRound(roundCounter, gamePlayers, 1);  // Play through next round; **gameOver should be set after playRound()**
                addToGameRounds(curRound, roundCounter);                                            // Add the round to the data structure that stores all rounds 
                gameOver = curRound.isGameOver();                                                   // check to see if game is over after round has been played
                
                if(gameOver){   // If after playRound(), game is over
                    cout << "Game is over! Someone has score above "<< MAXSCORE << endl; 
                    break; // Get out of while loop
                }
            } // Ends while
            */
            break; 
        case 'r':
            break;  
        case 'q':
            break; 
        default:
            cout << "Something went wrong." << endl; 
            break; 
    }   // Ends switch
    return 0;
}   // Ends Main




