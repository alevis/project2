/* 
    Player Class
    edited by Carl Senecal 10/23/14
*/
#include <iostream> 
#include "Player.h"						
#include "Card.h"

using namespace std; 

// CONSTRUCTOR/DESTRUCTOR DEFINITIONS
Player::Player(string pName, char h){
    name = pName;
    if(h == 'h' ||h == 'H') isHuman = true;
		else isHuman = false;
		
	leader = false; 
	has2Clubs - false;
	hasPlayed = false; 
	trickWinner; 
	
    rScore = 0;
    tScore = 0;
}
Player::~Player(){}

// METHOD DEFINITIONS
std::vector<Card> Player::getHand() {
    return hand;
}

void Player::toHand(Card newCard){
    hand.push_back(newCard);
}

/*
void Player::choose3(){
	if(human){
		gameDisplay.select3(this);		
	}
	else{
		chosen3 = playerAI.selectCardsToSwap(Hand);
		removeCard(0);
		removeCard(0);
		removeCard(0);
	}
}

void Player::choToHand(){
	for(int i = 0; i < 3; i++)
		hand.push_back(chosen3.at(i));
}

Card Player::removeCard(int picked){
	Card returnCard(1, 1, 0);
	vector<Card> newHand;
	bool met = false;
	for(int j = 0; j < hand.size()-1; j++) {
		if (picked == j){
			met = true;
			returnCard = hand.at(j);
		}
		if(met) {
			if (Hand.size() > 1)			
				newHand.push_back(hand.at(j+1));
		}
		else {
			newHand.push_back(hand.at(j));
		}
	}
	hand = newHand;
	return returnCard;
}


Card Player::playCard(std::vector<Card> tableCards){
	Card returnCard(1, 1, 0);
	if (human == true){
		returnCard = gameDisplay.soloPick(this);
	}
	else{
		int returnIndex = playerAI.selectCard(Hand, tableCards);
		returnCard = Hand.at(returnIndex);
		removeCard(returnIndex);
	}
	return returnCard;
}
*/