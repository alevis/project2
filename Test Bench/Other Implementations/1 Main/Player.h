#include <vector>
#include <string>
#include "Card.h"

using namespace std;

#ifndef PLAYER_H
#define PLAYER_H

class Player{
    public:
        //Attribute  
        string name;
        bool isHuman;
        //Gameplay
        vector<Card> hand;	        // Player's hands
	    vector<Card> chosen3;       // During swap, 3 cards go here
        bool leader;
        bool has2Clubs;
        bool hasPlayed; 
        bool trickWinner;
        //Scoring
        int rScore;             // A player's round score
        int tScore;             // A player's total score
        
        //Constructor/Destructor
        Player(string newName, char h);
        ~Player();
        
        //Method Declarations 
        vector<Card> getHand();	    // Returns player hand
        void toHand(Card newCard);  // Push a card onto player's hand
        
        /* Implemented with display
        void choose3();             // Player selects 3 cards from display
        void choToHand();           // During swap, puts 3 cards from chosen3 back into 
    	Card removeCard(int picked);    
    	Card playCard(vector<Card> tableCards);  // Put card on table
    	*/
    	    
};

#endif