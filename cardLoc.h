#ifndef CARDLOC_H
#define CARDLOC_H

struct cardLoc {
	int Left;
	int Right;
	int Bottom;
	int Top;
	int cardPosition;
	bool cardDeleted;
};

#endif
