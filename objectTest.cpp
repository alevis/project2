//Run this file to test Card and Deck classes.
#include <iostream>
#include "Card.h"
#include "Deck.h"

using namespace std;

main(){
    
    /* Main driver function for deck and card classes
        Should compile file in cloud9
        do:
        g++ -o test mainTester.cpp
        
        followed by
        ./test
        
    */
    
    Card card(1,2,0);
    
    cout<<"Successfully compiled!"<<endl;
    
    return 0;
    
}
